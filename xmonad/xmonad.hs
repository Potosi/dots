import XMonad
import XMonad.Util.EZConfig
import XMonad.Hooks.ManageDocks
import XMonad.Actions.CycleWS
import XMonad.Actions.NoBorders
import XMonad.Layout.ResizableThreeColumns
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile
import System.Exit
import XMonad.StackSet as W

myConf = def
    { modMask            = mod4Mask
    , layoutHook         = myLayoutHook
    , startupHook        = spawn ". ~/.config/autostart.sh"
    , XMonad.borderWidth = myBorderWidth
    , normalBorderColor  = "#1e1e1e"
    , focusedBorderColor = "#b6a0ff"
    } `additionalKeysP`
        [ ("M-q", spawn "xmonad --restart")
        , ("M-S-<Return>", spawn "st")
        , ("M-p", spawn "dmenu_run -l 10 -nb '#000000' -sb '#b6a0ff' -sf '#1e1e1e' -fn 'Roboto Mono'")
        , ("M-S-x", spawn  "slock")
        , ("M-S-q", io exitSuccess)

        , ("M-<Space>", sendMessage NextLayout)
        , ("M-b", sendMessage ToggleStruts)
        , ("M-S-b", withFocused toggleBorder )
        , ("M-m", windows W.focusMaster)
        , ("M-n", windows W.focusDown)
        , ("M-<Return>", windows W.swapMaster)
        , ("M-S-j", windows W.swapDown)
        , ("M-S-k", windows W.swapUp)
        , ("M-h", sendMessage Shrink)
        , ("M-l", sendMessage Expand)
        , ("M-i", sendMessage MirrorExpand)
        , ("M-o", sendMessage MirrorShrink)
        , ("M-t", withFocused $ windows . W.sink)
        , ("M-,", sendMessage (IncMasterN 1))
        , ("M-.", sendMessage (IncMasterN (-1)))
        , ("M-<Right>", nextScreen)
        , ("M-<Left>", prevScreen)

        ]

myBorderWidth :: Dimension
myBorderWidth = 2

myLayoutHook = avoidStruts (ResizableTall 1 (3/100) (1/2) [] ||| ResizableThreeColMid 1 (3/100) (1/2) [] ||| noBorders Full)

main :: IO ()
main = do
  xmonad $ docks myConf
