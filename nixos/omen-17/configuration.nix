{ lib, config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../musnix
    ];

  musnix = {
    enable = true;
    alsaSeq.enable = true;
    kernel.realtime = true;
  };

  boot = {
    kernelModules = [ "snd-seq" "snd-rawmidi" ];
    kernelParams = [ "threadirq" ];
    blacklistedKernelModules = [ "snd_pcsp" "snd_hda_intel" ];
  loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
    };
  };

  security.rtkit.enable = true;
  security.doas.enable = true;
  security.sudo.enable = false;
  security.doas.extraRules = [{
    users = [ "potosi" ];
    keepEnv = true;
    persist = true;  
  }];

  sound.enable = false;

  networking = {
    firewall.enable = false;
    hostName = "potosi_nixos";
    wireless.iwd.enable = true;
  };

  time.timeZone = "Europe/Madrid";

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };

  services = {
	udev = {
	  extraRules = ''
            KERNEL=="rtc0", GROUP="audio"
            KERNEL=="hpet", GROUP="audio"
          '';
	};
	xserver = {
          enable = true;
	  layout = "us,es";
	  videoDrivers = ["nvidia"];
	  displayManager.startx.enable = true;
          displayManager.lightdm.enable = false;
          windowManager.dwm.enable = true;
	  libinput = {
	    enable = true;
	    mouse.accelProfile = "flat";
	  };
	  excludePackages = with pkgs; [ xterm ];
	};
	pipewire = {
	  enable = true;
	  alsa.enable = true;
	  alsa.support32Bit = true;
	  pulse.enable = false;
	  jack.enable = false;
	};
        jack = {
          jackd.enable = false;
          alsa.enable = false;
          loopback = {
            enable = true;
          };
        };
	printing = {
	  enable = true;
	  drivers = [ pkgs.gutenprint pkgs.hplipWithPlugin ];
	};
	openssh.enable = true;
	cron.enable = false;
	autorandr.enable = true;
  };

  programs.ssh.startAgent = true;
  programs.slock.enable = true;

  users.users.potosi = {
    isNormalUser  = true;
    home  = "/home/potosi";
    description  = "Miguel Blasco";
    extraGroups  = [ "wheel" "jackaudio" "audio" "video" "libvirtd" ];
  };

  nixpkgs.config = {
     allowUnfree = true;
    packageOverrides = pkgs: {
      vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
    };

  };

  hardware = {
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
      extraPackages = with pkgs; [
        intel-media-driver # LIBVA_DRIVER_NAME=iHD
        vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
        vaapiVdpau
        libvdpau-va-gl
      ];
    };
    nvidia = {
      modesetting.enable = true;
      open = false;
      nvidiaSettings = true;
      powerManagement.enable = lib.mkForce false;
      prime = {
	offload = {
	enable = lib.mkForce false;
	enableOffloadCmd = true;
      };
	reverseSync.enable = true;
	intelBusId = "PCI:0:2:0";
    	nvidiaBusId = "PCI:1:0:0";
      };
    };
  };

  fonts.fonts = with pkgs; [
    scientifica
    roboto-mono
    (nerdfonts.override { fonts = [ "Terminus" "Monoid" "3270" "Mononoki" "CascadiaCode" "RobotoMono" ]; })
  ];

  environment.systemPackages = with pkgs; [
    (st.overrideAttrs (oldAttrs: rec {
      patches = [
        (fetchpatch {
          url = "https://st.suckless.org/patches/scrollback/st-scrollback-ringbuffer-0.8.5.diff";
          sha256 = "0xxwgkgpzc7s8ad0pgcwhm5hqyh2wy56a9yrxid68xm0np2g6m5h";
        })
	(fetchpatch {
          url = "https://st.suckless.org/patches/anysize/st-anysize-20220718-baa9357.diff";
          sha256 = "1ym5d2f85l3avgwf9q93aymdg23aidprqwyh9s1fdpjvyh80rvvq";
        })
      ];
      configFile = writeText "config.h" (builtins.readFile ./st-config.h);
      colorsH = writeText "colors.h" (builtins.readFile ./colors.h);
      postPatch = "${oldAttrs.postPatch}\n cp ${configFile} config.def.h \n cp ${colorsH} colors.h";
    }))
    (dwm.overrideAttrs (oldAttrs: rec {
      patches = [
        (fetchpatch {
          url = "https://dwm.suckless.org/patches/restartsig/dwm-restartsig-20180523-6.2.diff";
          sha256 = "0s9gfimv3m233bc9iw8w0j0ma0qank3zxffx5630qasmv90jyky7";
        })
        (fetchpatch {
          url = "https://dwm.suckless.org/patches/noborder/dwm-noborderfloatingfix-6.2.diff";
          sha256 = "114xcy1qipq6cyyc051yy27aqqkfrhrv9gjn8fli6gmkr0x6pk52";
        })
	(fetchpatch {
          url = "https://dwm.suckless.org/patches/steam/dwm-steam-6.2.diff";
	  sha256 = "1ld1z3fh6p5f8gr62zknx3axsinraayzxw3rz1qwg73mx2zk5y1f";
	})
      ];
      configFile = writeText "config.h" (builtins.readFile ./dwm-config.h);
      colorsH = writeText "colors.h" (builtins.readFile ./colors.h);
      postPatch = "${oldAttrs.postPatch}\n cp ${configFile} config.def.h \n cp ${colorsH} colors.h";
    }))
    (slstatus.overrideAttrs (oldAttrs: rec {
      configFile = writeText "config.h" (builtins.readFile ./slstatus-config.h);
      postPatch = "cp ${configFile} config.def.h";
    }))
    (slock.overrideAttrs (oldAttrs: rec {
      configFile = writeText "config.h" (builtins.readFile ./slock-config.h);
      colorsH = writeText "colors.h" (builtins.readFile ./colors.h);
      postPatch = "${oldAttrs.postPatch}\n cp ${configFile} config.def.h \n cp ${colorsH} colors.h";
    }))

    dmenu
    st
    git
    ranger
    neofetch
    slock
    slstatus
    jack2
  ];

  system.copySystemConfiguration = true;

  system.stateVersion = "23.05";

}


