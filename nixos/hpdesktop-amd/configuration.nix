{ lib, config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot = {
    kernelModules = [ "amdgpu" ];
    loader.grub.enable = true;
    loader.grub.device = "/dev/sda";
    loader.grub.useOSProber = false;
  };

  security.doas.enable = true;
  security.sudo.enable = false;
  security.doas.extraRules = [{
    users = [ "potosi" ];
    keepEnv = true;
    persist = true;  
  }];

  sound.enable = false;

  networking = {
    firewall.enable = false;
    hostName = "hpdesktop";
    wireless.iwd.enable = true;
  };

  time.timeZone = "Europe/Madrid";

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };

  services = {
	xserver = {
          enable = true;
	  layout = "us,es";
	  videoDrivers = ["amdgpu"];
	  displayManager.startx.enable = true;
          displayManager.lightdm.enable = false;
          windowManager.dwm.enable = true;
	  libinput = {
	    enable = true;
	    mouse.accelProfile = "flat";
	  };
	  excludePackages = with pkgs; [ xterm ];
	};
	pipewire = {
	  enable = true;
	  alsa.enable = true;
	  alsa.support32Bit = true;
	  pulse.enable = false;
	  jack.enable = false;
	};
	printing = {
	  enable = true;
	  drivers = [ pkgs.gutenprint ];
	};
	openssh.enable = true;
	cron.enable = false;
	autorandr.enable = true;
  };

  programs.ssh.startAgent = true;
  programs.slock.enable = true;
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "steam"
    "steam-original"
    "steam-run"

];

  users.users.potosi = {
    isNormalUser  = true;
    home  = "/home/potosi";
    description  = "Miguel Blasco";
    extraGroups  = [ "wheel" "audio" "video" ];
  };

  hardware = {
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };
  };

  fonts.fonts = with pkgs; [
    scientifica
    roboto-mono
    (nerdfonts.override { fonts = [ "Terminus" "Monoid" "3270" "Mononoki" "CascadiaCode" "RobotoMono" ]; })
  ];

  environment.systemPackages = with pkgs; [
    (st.overrideAttrs (oldAttrs: rec {
      patches = [
        (fetchpatch {
          url = "https://st.suckless.org/patches/scrollback/st-scrollback-ringbuffer-0.8.5.diff";
          sha256 = "0mgsklws6jsrngcsy64zmr604qsdlpd5pqsa3pci7j3gn8im4zyw";
        })
	(fetchpatch {
          url = "https://st.suckless.org/patches/anysize/st-anysize-20220718-baa9357.diff";
          sha256 = "1ym5d2f85l3avgwf9q93aymdg23aidprqwyh9s1fdpjvyh80rvvq";
        })
      ];
      configFile = writeText "config.h" (builtins.readFile ./st-config.h);
      colorsH = writeText "colors.h" (builtins.readFile ./colors.h);
      postPatch = "${oldAttrs.postPatch}\n cp ${configFile} config.def.h \n cp ${colorsH} colors.h";
    }))
    (dwm.overrideAttrs (oldAttrs: rec {
      patches = [
        (fetchpatch {
          url = "https://dwm.suckless.org/patches/restartsig/dwm-restartsig-20180523-6.2.diff";
          sha256 = "0s9gfimv3m233bc9iw8w0j0ma0qank3zxffx5630qasmv90jyky7";
        })
        (fetchpatch {
          url = "https://dwm.suckless.org/patches/noborder/dwm-noborderfloatingfix-6.2.diff";
          sha256 = "114xcy1qipq6cyyc051yy27aqqkfrhrv9gjn8fli6gmkr0x6pk52";
        })
	(fetchpatch {
          url = "https://dwm.suckless.org/patches/steam/dwm-steam-6.2.diff";
	  sha256 = "1ld1z3fh6p5f8gr62zknx3axsinraayzxw3rz1qwg73mx2zk5y1f";
	})
      ];
      configFile = writeText "config.h" (builtins.readFile ./dwm-config.h);
      colorsH = writeText "colors.h" (builtins.readFile ./colors.h);
      postPatch = "${oldAttrs.postPatch}\n cp ${configFile} config.def.h \n cp ${colorsH} colors.h";
    }))
    (slstatus.overrideAttrs (oldAttrs: rec {
      configFile = writeText "config.h" (builtins.readFile ./slstatus-config.h);
      postPatch = "cp ${configFile} config.def.h";
    }))
    (slock.overrideAttrs (oldAttrs: rec {
      configFile = writeText "config.h" (builtins.readFile ./slock-config.h);
      colorsH = writeText "colors.h" (builtins.readFile ./colors.h);
      postPatch = "${oldAttrs.postPatch}\n cp ${configFile} config.def.h \n cp ${colorsH} colors.h";
    }))

    dmenu
    st
    git
    ranger
    neofetch
    slock
    slstatus
  ];

  system.copySystemConfiguration = true;

  system.stateVersion = "23.05";

}


