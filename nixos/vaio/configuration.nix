{ lib, config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.useOSProber = true;

  networking = {
    hostName = "vaio-nixos";
    wireless.iwd.enable = true;
    firewall.enable = false;
  };

  time.timeZone = "Europe/Madrid";

  i18n.defaultLocale = "en_US.UTF-8";

  services = {
    xserver = {
      enable = true;
      layout = "es";
      videoDrivers = ["nouveau"];
      xkbVariant = "";
      displayManager.startx.enable = true;
      displayManager.lightdm.enable = false;
      windowManager.dwm.enable = true;
      libinput.enable = true;
    };
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = false;
      jack.enable = false;
    };
    printing = {
      enable = true;
      drivers = with pkgs; [ gutenprint hplipWithPlugin ];
    };
    openssh.enable = true;
    cron.enable = false;
  };

  hardware = {
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };
  };

  console.keyMap = "es";

  users.users.potosi = {
    isNormalUser = true;
    description = "potosi";
    extraGroups = [ "video" "audio"  "wheel" ];
  };

  # Allow unfree packages
  nixpkgs.config = {
    allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) [
	"hplip"
	"hplipWithPlugin"
      ];
  };

  fonts.fonts = with pkgs; [
    scientifica
    roboto-mono
    (nerdfonts.override {fonts = [ "Monoid" "3270" "Mononoki" "CascadiaCode" "RobotoMono" ]; })
  ];

  environment.systemPackages = with pkgs; [
    (st.overrideAttrs (oldAttrs: rec {
      patches = [
        (fetchpatch {
          url = "https://st.suckless.org/patches/scrollback/st-scrollback-ringbuffer-0.8.5.diff";
          sha256 = "0xxwgkgpzc7s8ad0pgcwhm5hqyh2wy56a9yrxid68xm0np2g6m5h";
        })
        (fetchpatch {
          url = "https://st.suckless.org/patches/anysize/st-anysize-20220718-baa9357.diff";
          sha256 = "1ym5d2f85l3avgwf9q93aymdg23aidprqwyh9s1fdpjvyh80rvvq";
        })
      ];
      configFile = writeText "config.h" (builtins.readFile ./st-config.h);
      colorsH = writeText "colors.h" (builtins.readFile ./colors.h);
      postPatch = "${oldAttrs.postPatch}\n cp ${configFile} config.def.h \n cp ${colorsH} colors.h";
    }))
    (dwm.overrideAttrs (oldAttrs: rec {
      patches = [
        (fetchpatch {
          url = "https://dwm.suckless.org/patches/restartsig/dwm-restartsig-20180523-6.2.diff";
          sha256 = "0s9gfimv3m233bc9iw8w0j0ma0qank3zxffx5630qasmv90jyky7";
        })
        (fetchpatch {
          url = "https://dwm.suckless.org/patches/noborder/dwm-noborderfloatingfix-6.2.diff";
          sha256 = "114xcy1qipq6cyyc051yy27aqqkfrhrv9gjn8fli6gmkr0x6pk52";
        })
        })
      ];
      configFile = writeText "config.h" (builtins.readFile ./dwm-config.h);
      colorsH = writeText "colors.h" (builtins.readFile ./colors.h);
      postPatch = "${oldAttrs.postPatch}\n cp ${configFile} config.def.h \n cp ${colorsH} colors.h";
    }))
    (slstatus.overrideAttrs (oldAttrs: rec {
      configFile = writeText "config.h" (builtins.readFile ./slstatus-config.h);
      postPatch = "cp ${configFile} config.def.h";
    }))

    scroll
    st
    dmenu
    slstatus
    slock
  ];

  system.stateVersion = "23.05"; # Did you read the comment?

}
