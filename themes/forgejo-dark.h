#define BLACK           "#111827"
#define RED             "#b91c1c"
#define GREEN           "#15803d"
#define YELLOW          "#ca8a04"
#define BLUE            "#2563eb"
#define PINK            "#db2777"
#define CYAN            "#0d9488"
#define GREY            "#515F70"

#define BLACK_LIGHT     "#1f2937"
#define RED_LIGHT       "#dc2626"
#define GREEN_LIGHT     "#16a34a"
#define YELLOW_LIGHT    "#eab308"
#define BLUE_LIGHT      "#3b82f6"
#define PINK_LIGHT      "#ec4899"
#define CYAN_LIGHT      "#14b8a6"
#define GREY_LIGHT      "#8C9CAF"

#define FOREGROUND      "#ffffff"
#define BACKGROUND      "#10161D"

#define FOREGROUND2     "#D2E0F0"
#define BACKGROUND2     "#1D262F"

#define ACCENT          "#FB923C"

#define INACTIVE_BORDER "#2b3642"

#define FONT_DWM        "Terminus:size=10"
#define FONT_ST         "Terminus:pixelsize=18:antialias=true:autohint=true"

/*I use ACCENT as active border color*/
