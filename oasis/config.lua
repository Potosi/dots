local sets = dofile(basedir..'/sets.lua')

return {
        builddir='out',

        prefix='',

        fs={
                {sets.core, sets.extra, sets.devel, sets.media,
		sets.desktop, 'bearssl', 'elftoolchain',
		'syslinux', 'efibootmgr', 'ncurses',
        },

        target={
                platform='x86_64-linux-musl',
                cflags='-Os -pipe',
                ldflags='-s -static',
        },

        host={
                cflags='-Os -pipe',
                ldflags='-s -static',
        },

        repo={
                path='../..',
                flags='',
                tag='tree',
                branch='oasis',
        },

        video_drivers={
		nouveau=true,
		intel=true,
	}
}
