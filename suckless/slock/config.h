#include "/home/potosi/projects/dots/suckless/colors.h"

/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nogroup";

static const char *colorname[NUMCOLS] = {
	[INIT] =   BACKGROUND,     /* after initialization */
	[INPUT] =        BLUE,     /* during input */
	[FAILED] =        RED,     /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;

