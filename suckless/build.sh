#!/bin/sh

set -e

if [ -n "$PREFIX" ]
then
        PREFIX=~/.local
fi

for dir in */; do

        if ! [ "$(ls -A "$dir"/source)" ]
        then
                git submodule update --init --recursive "$dir"/source
        fi

        if ! [ -f "$dir"/source/colors.h ]
        then
                ln -s ../../colors.h "$dir"/source/
        fi
        # I do this instead of '#include "../../colors.h"' because I have
        # to make the config 'package-manager compatible'.

        if ! [ -f "$dir"/source/config.h ]
        then
                ln -s ../config.h "$dir"/source/
        fi

        cd "$dir"/source

        if [ -d ../patches/ ]; then
                for patch in ../patches/*; do
                        patch -p1 < "$patch"
                done
        fi

        make -j"$(nproc)"
        make install PREFIX="$PREFIX"

        cd "$OLDPWD"
done
