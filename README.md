# dots

![](screenshots/1.png)

I make use of the following patches:

### dwm

* restartsig
* steam
* noborderfloatingfix

### st
* scrollback-ringbuffer
* anysize

## Setup

* I use dwm as my only window manager.

* I use dmenu to launch programs.

* I use st as my only terminal.

* I use the "forgejo-dark" colorscheme from codeberg.

* I use Gentoo on my main machine and OpenBSD on my old server.

# TODO

(Not in order of priority)

- [ ] Add Makefile instead of build script
- [x] Use XMonad keyboard shortcuts for dwm
- [x] Update suckless software
- [ ] Add Emacs config
- [x] Change colorscheme
- [ ] Improve colorscheme implementation (not working properly on st)
- [x] Do slock config
- [x] Add Gentoo config
- [ ] Perhaps restructure repo
- [ ] Improve kernel configs
- [ ] Add modus theme
- [ ] Create ebuilds for OSS4 and slstatus

#### Notes:

* Files in 'patches/' originate from [suckless.org](https://suckless.org) are **not** my work.

* The colorschemes are **not** my work. However, their implementation into the config files are.
  - [Modus-themes-repo](https://git.sr.ht/~protesilaos/modus-themes)
  - [Codeberg-forgejo-dark.css](https://codeberg.org/Codeberg/forgejo/src/branch/codeberg-1.19/web_src/css/themes/theme-forgejo-dark.css)

* The configs for suckless software are based on the projects' `config.def.h`.

* I use my own fork of oasislinux. See:
  - [Original](https://github.com/oasislinux/oasis)
  - [My fork](https://codeberg.org/Potosi/oasis)
